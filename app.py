
import telegram.ext
from Bot.valid_url import valid

from Bot.bot import Bot
from Bot.link_provider import return_api_txt

TOKEN = return_api_txt(4)

def start(update, context):
    update.message.reply_text("""Yt-Hub allows you to convert & download video from YouTube Video, Dailymotion, Youku, etc. to Mp3, Mp4 in HD quality. "Yt-Hub supports downloading all video formats such as: MP4,  3GP,  MP3, etc. You can easily download for free thousands of videos from YouTube

Examples command -
            
    MP3 -
        
        /music youtube-link
    
    MP4 -
        
        /video youtube-link
""")

def link(update, context):
    try:
        yt_link = context.args[0]
        k = valid(yt_link)
        if isinstance(k, int):
            sess = Bot(yt_link, key="mp3")
            value = sess.validate_link()

            update.message.reply_text(f"""
PHOTO   = {value["image"]}

TITLE    = {value['title']}

CHANNEL  = {value["channel"]}

MP3-LINK ({value['size']}) =  {value["download"]}

TOTAL-TIME = {value["time"]} seconds

""")

        else:
            update.message.reply_text(k)
    except:
        update.message.reply_text('''Pleace provide valid Command
valid command = /link YT-LINK ''')


def link_2(update, context):
    try:
        yt_link = context.args[0]
        # print(yt_link)
        k = valid(yt_link)
        if isinstance(k, int):
            update.message.reply_text(
                "🤞 wait for second 🤏 finding best(🍻) and fast(⚡️) servers ")

            sess = Bot(yt_link, key="mp4")
            value = sess.validate_link()

            result = f"""
PHOTO   = {value["image"]}

TITLE    = {value['title']}

CHANNEL  = {value["channel"]}
"""
            for i, j in value['download_link'].items():
                result += f"\n {i} = {j}"
                result += "\n"

            result += f"\n TOTAL-TIME = {value['time']} seconds"

            update.message.reply_text(result)

        else:
            update.message.reply_text(k)
    except:
        update.message.reply_text('''Pleace provide valid Command
valid command = /video YT-LINK ''')


updater = telegram.ext.Updater(TOKEN, use_context=1)

dis = updater.dispatcher

dis.add_handler(telegram.ext.CommandHandler("start", start))
dis.add_handler(telegram.ext.CommandHandler("music", link))
dis.add_handler(telegram.ext.CommandHandler("video", link_2))

updater.start_polling()
updater.idle()

