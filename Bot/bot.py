
from validators import url
from json import loads
from re import search
from .link_provider import Link_provider, Featch, thread_pool_executor,return_api_txt


def short_url(n):
    data_ = Featch.post(
        return_api_txt(3), data={"URL": n})
    return data_.json()

class Bot(object):

    __slots__ = ("link", "regx", "key", "result", "return_dict")

    def __init__(self, link: str, key: str = "") -> None:
        self.link = link
        self.key = key.lower()

    def check_link_proper(self, fun, v):
        data = fun(value=v)
        x = (data["dlink"].split(".xyz/?file=")[0][8:].split(".")[1][:6])
        if(x == "dlmate"):
            del x, data
            return self.check_link_proper(fun, v)
        else:
            del x
            return data

    def add_size(self, n, v):
        if "p" in v:
            v = v.replace("HFR", "")
            # print(v)
            all_p = n["links"]['mp4'][v.replace(" ", '')]['size']
        else:
            v = v.replace("HFR", "")
            all_p = n["links"]["mp4"][f"{v.replace(' ','')}p"]['size']
        return all_p

    def validate_link(self):
        self.regx = "^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$"

        if url(self.link) and search(self.regx, self.link):
            del self.regx

            if self.key == "mp3" or self.key == "audio":
                self.result = Link_provider(self.link)
                mp_3 = self.result.parse_json()
                send = {"vid": mp_3["vid"],
                        "k": mp_3["links"]['mp3']['mp3128']["k"]}

                download_data = self.check_link_proper(self.result.get_download_link,
                                                       send)
                short = (thread_pool_executor(
                    short_url, download_data["dlink"]))['short']

                self.return_dict = dict(
                    title=download_data["title"], image=f"https://i.ytimg.com/vi/{download_data['vid']}/0.jpg", download=short, channel=mp_3["a"],
                    time=mp_3['t'], size=mp_3["links"]['mp3']['mp3128']['size'])

                del short, download_data, send, mp_3, self.result

                return self.return_dict

            if self.key == "mp4":
                self.result = Link_provider(self.link)
                mp_4 = self.result.parse_json()

                list_of_p = list(mp_4['links']['mp4'].keys())
                result_value = []

                for i in range(len(list_of_p)):
                    send = {"vid": mp_4["vid"],
                            "k": mp_4["links"]['mp4'][list_of_p[i]]["k"]}
                    data_add = self.check_link_proper(
                        self.result.get_download_link, send)

                    del (data_add['status'], data_add['mess'], data_add['c_status'],
                         data_add['ftype'])

                    result_value.append(data_add)
                del list_of_p
                links_value = []
                for i in range(len(result_value)):
                    links_value.append((
                        f'''{result_value[i]["fquality"]} ({self.add_size(mp_4, result_value[i]['fquality'])})''',
                        (thread_pool_executor(
                            short_url, result_value[i]["dlink"]))['short']
                    ))

                out_dict = dict(title=result_value[0]['title'],
                                image=f"https://i.ytimg.com/vi/{mp_4['vid']}/0.jpg",
                                download_link=dict(links_value),
                                time=mp_4["t"],
                                channel=mp_4["a"],
                                )
                del mp_4, links_value, result_value, self.result

                return out_dict

        else:
            return "Invalid Url"