from re import search
from validators import url


def valid(link):
    regx = "^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$"

    if(url(link)):
        if search(regx, link):
            return 1
        else:
            return "Invalid Youtube Url"
    else:
        return "Invalid Url"
