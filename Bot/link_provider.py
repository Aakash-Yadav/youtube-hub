from requests import Session
from json import dumps, loads
from .hash import convert_to_decrypt
from concurrent.futures import ThreadPoolExecutor

Featch = Session()


def thread_pool_executor(function, value):
    with ThreadPoolExecutor() as exe:
        data = exe.submit(function, value)
    return data.result()


def return_api_txt(n: int):
    with open('Bot/private.json', 'r') as F:
        data = loads(F.read())
    url_key = data[list(data.keys())[n-1]][2:].encode()
    return convert_to_decrypt(url_key)


class Link_provider(object):

    __slots__ = ("youtube_url", "key_type", "__base_url_1", "__base_url_2",
                 "search_index")

    def __init__(self, youtube_url) -> None:

        self.youtube_url = youtube_url
        self.__base_url_1 = return_api_txt(1)
        self.__base_url_2 = return_api_txt(2)

    def __convert_an_youtube_videos(self, v=None):
        return Featch.post(self.__base_url_1,
                           data={"q": self.youtube_url,
                                 "vt": "home"}).json()

    def __get_link(self, data):
        return Featch.post(self.__base_url_2,
                           data=data).json()

    def parse_json(self):
        result_dict = dict()
        json_response = thread_pool_executor(
            self.__convert_an_youtube_videos, 1)

        for i in ["status", "mess", "p"]:
            del json_response[i]

        for i in list((json_response['links']["mp4"]).keys())[:-1]:
            result_dict.update(
                {json_response["links"]["mp4"][i]["q"]: json_response["links"]["mp4"][i]})

        json_response["links"]["mp4"] = result_dict
        del result_dict

        return json_response

    def get_download_link(self, json_response=None, Key=None, value=None):

        send = None

        if not value:

            if Key == "mp3128" or Key == "3gp":
                send = (json_response["links"][Key])
            else:
                send = (json_response["links"]["mp4"][Key])

            download_link_json = thread_pool_executor(
                self.__get_link, {"vid": json_response["vid"], "k": send["k"]})
            del send, json_response, Key
        else:

            download_link_json = thread_pool_executor(self.__get_link, value)

            return download_link_json
