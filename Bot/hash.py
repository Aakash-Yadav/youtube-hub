from cryptography.fernet import Fernet 


x= open('Bot/key.key','rb')
data = x.read()
x.close()

f = Fernet(data)

def convert_to_decrypt(n):
    value = (f.decrypt(n))
    return ''.join(map(chr, value))